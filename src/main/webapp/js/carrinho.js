"use strict";

let addActionUrl = null;
function setAdicionarCarrinho(url) {
	addActionUrl = url;
}

async function adicionarCarrinho(produtoId) {
	const data = new URLSearchParams();
	data.set("produto", produtoId);
	const response = await fetch(addActionUrl, {
		method: 'POST',
		body: data
	});
	
	if (response.status == 200) {
		showMessage('Produto adicionado');
	}
}

function showMessage(message) {
	const span = document.querySelector('#async-message');
	span.textContent = message;
	span.classList.remove('hidden');
	setTimeout(() => {span.classList.add('hidden');}, 1000);
}